<!DOCTYPE html>
<html lang="en">

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="my-4 col-12">
                <h1 class="float-left">Daftar Buku</h1>
                <a class="btn btn-primary float-right mt-2" href="{{url('/dftr/create')}}" role="button">Tambah Buku</a>
            </div>
            <div class="col-12">
                <table class="table table-stripped">
                    <thead class="thead-primary">
                        <tr>
                            <th class="text-center">No</th>
                            <th>Tipe Buku</th>
                            <th>Nama Buku</th>
                            <th>Penerbit</th>
                            <th>Kategori</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($dftr as $d)
                        <tr>
                            <td scope="row">{{ $loop->iteration }}</td>
                            <td>{{ $d->tipe_buku }}</td>
                            <td>{{ $d->nama_buku }}</td>
                            <td>{{ $d->penerbit }}</td>
                            <td>{{ $d->name_category }}</td>
                            <td>
                                <a href="/dftr/edit/{{$d -> id}}" class="badge badge-success">Edit</a>
                                <a href="/dftr/delete/{{$d->id }}" class="badge badge-danger" onclick="return confirm('Apakah anda yakin untuk menghapus data ini?')">Delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="text-center">
        {!! $dftr->links() !!}
    </div>
</body>

</html>