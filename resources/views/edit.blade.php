<!DOCTYPE html>
<html lang="en">

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-12 mt-3">
                <h3>Form Edit Buku</h3>
                <br><br>
                <a class="btn btn-primary" href="{{url('/')}}" role="button">Daftar Buku</a>
                <br><br><br>
                @foreach($dftr as $d)
                <form action="/dftr/update" method="post">
                    @csrf
            </div>
            <input type="hidden" name="id" value="{{ $d->id }}">
            <div class="form-group">
                <label for="type">Tipe Buku</label>
                <input class="form-control" type="text" name="type" id="type" placeholder="Masukkan Nama Buku" required="required" value="{{ $d->tipe_buku }}">
            </div>
            <div class="form-group">
                <label for="name">Nama Buku</label>
                <input class="form-control" type="text" name="name" id="name" placeholder="Masukkan Judul Buku" required="required" value="{{ $d->nama_buku }}">
            </div>
            <div class="form-group">
                <label for="publisher">Penerbit</label>
                <input class="form-control" type="text" name="publisher" id="publisher" placeholder="Masukkan Penerbit Buku" required="required" value="{{ $d->penerbit }}">
            </div>
            <div class="form-group">
                <label class="control-label" for="category">Usia</label>
                <select name="name_category" id="name_category" class="form-control">
                    @foreach($category as $Category)
                    <option value="{{$Category->name_category}}"
                    @if ($Category->name_category === $Category->name_category)
                        selected
                        @endif
                        > {{ $Category->name_category }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group float-right">
                <button class="btn btn-lg btn-primary" type="submit">Submit</button>
            </div>
            </form>
            @endforeach
        </div>
    </div>
    </div>

</body>

</html>