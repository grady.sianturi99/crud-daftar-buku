<!DOCTYPE html>
<html lang="en">

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-12 mt-3">
                <h3>Form Menambah Buku</h3>
                <br><br>
                <a class="btn btn-primary" href="{{url('/')}}" role="button">Daftar Buku</a>
                <br><br><br>
                <form action="/dftr/store" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="type">Tipe Buku</label>
                        <input class="form-control class @error('type') is-invalid @enderror" type="text" name="type" id="type" placeholder="Masukkan Nama Buku">
                        @error('type')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="name">Nama Buku</label>
                        <input class="form-control class @error('name') is-invalid @enderror" type="text" name="name" id="name" placeholder="Masukkan Judul Buku">
                        @error('name')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="publisher">Penerbit</label>
                        <input class="form-control @error('publisher') is-invalid @enderror" type="text" name="publisher" id="publisher" placeholder="Masukkan Penerbit Buku">
                        @error('publisher')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="category">Usia</label>
                        <select name="name_category" id="" class="form-control">
                            @foreach($category as $Category)
                            <option value="{{$Category->name_category}}"> {{ $Category->name_category }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group float-right">
                        <button class="btn btn-lg btn-primary" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</body>

</html>