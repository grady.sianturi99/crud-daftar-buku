<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDftrTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dftr', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipe_buku');
            $table->string('nama_buku');
            $table->string('penerbit');
            $table->string('name_category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dftr');
    }
}
