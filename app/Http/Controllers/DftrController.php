<?php

namespace App\Http\Controllers;

use App\category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\dftr;

class DftrController extends Controller
{
    public function index()
    {

        $dftr = DB::table('dftr')->paginate(5);
        return view('index', ['dftr' => $dftr]);
    }

    public function create()
    {
        $category = Category::all();

        return view('create', compact('category'));
    }

    public function store(Request $request)
    {

        $request->validate([
            'type' => 'required',
            'name' => 'required',
            'publisher' => 'required',
            'name_category' => 'required'
        ]);

        DB::table('dftr')->insert([
            'tipe_buku' => $request->type,
            'nama_buku' => $request->name,
            'penerbit' => $request->publisher,
            'name_category' => $request->name_category
        ]);

        return redirect('/dftr');
    }

    public function edit($id)
    {

        $category = Category::all();
        $dftr = DB::table('dftr')->where('id', $id)->get();
        return view('edit', compact('dftr','category'));
    }


    public function update(Request $request)
    {
        DB::table('dftr')->where('id', $request->id)->update([
            'tipe_buku' => $request->type,
            'nama_buku' => $request->name,
            'penerbit' => $request->publisher,
            'name_category' => $request->name_category
        ]);
        return redirect('/dftr');
    }

    public function delete($id)
    {
        DB::table('dftr')->where('id', $id)->delete();
        return redirect('/dftr');
    }
}
