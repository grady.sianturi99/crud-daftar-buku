<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    protected $table = 'category';
    protected $fillable = ['id_category', 'name_category'];

    public function dftr()
    {
        return $this->belongsTo('App\dftr');
    }
}
