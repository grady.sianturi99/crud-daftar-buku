<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dftr extends Model
{
    protected $table = 'dftr';
    protected $primaryKey = 'id';
    protected $fillable = ['tipe_buku','nama_buku','penerbit','name_category'];

    public function category()
    {
        return $this->hasMany('App\category');
    }
}
