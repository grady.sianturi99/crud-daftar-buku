<?php

use App\Http\Controllers\DftrController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');/
//});
Route::get('/','DftrController@index');

Route::get('/dftr','DftrController@index');

Route::get('/dftr/create','DftrController@create');

Route::post('dftr/store','DftrController@store');

Route::get('/dftr/edit/{id}','DftrController@edit');

Route::post('/dftr/update','DftrController@update');

Route::get('/dftr/delete/{id}','DftrController@delete');